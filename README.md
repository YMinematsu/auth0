# Auth0 Tenant Setting Files

# Setup

```bash
$ brew install node
$ npm install # or yarn install
```

# How to Use

## Export

```bash
$ npx a0deploy export -c export-config.json -f yaml -o exported
```

## Import

```bash
$ npx a0deploy import -c import-config.json -i tenant.yaml
```