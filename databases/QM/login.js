function login(email, password, callback) {
  const request = require('request');

  request.post({
    url: 'https://devqms.sysmex-nss.jp/qm/common/api/login',
    headers: {
        'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      "account": email,
      "password": password
    })
  }, (err, response, body) => {
    console.log("body" + body);
    const user = JSON.parse(body);
    if (user.error) return callback(new ValidationError(user.error.status, user.error.errorCode));
    if (response.statusCode === 401) return callback();
    
    callback(null, {
      name: email,
      user_id: email,
      password: password,
      Loginid: email
    });
  });
}
