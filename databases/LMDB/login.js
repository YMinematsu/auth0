function login(email, password, callback) {
    const request = require('request');

    request.post(
        {
            url: 'https://qa.sysmex-nss.jp/sso/webapi/api/LogIn',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: 'client_id=NssWebClient&grant_type=password&password=' + password + '&userName=' + email
        },
        (err, response, body) => {
            console.log('body' + body);
            const user = JSON.parse(body);
            if (user.error) return callback(err);
            if (response.statusCode === 401) return callback();
            
            callback(null, {
                name: email,
                user_id: user.userName,
                Loginid: user.userName,
                Identities: user.identities,
              	email:email
            });
        }
    );
}
