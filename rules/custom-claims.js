function (user, context, callback) {
  const namespace = 'https://www.sysmex.co.jp/';
  
  context.idToken[namespace + 'Loginid'] = user.identities[0].user_id;
  context.idToken[namespace + 'Identity'] = user.identities; 
  
  const identities = [];
  for (let identity of user.identities) {
    identities.push({
      user_id: identity.user_id,
      connection: identity.connection
    });
  }
  context.idToken[namespace + 'identities'] = identities;
  
  callback(null, user, context);
}