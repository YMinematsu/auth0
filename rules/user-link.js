function (user, context, callback) {
  const request = require('request');
  const primaryConnectionName = 'LMDB';
  const secondaryConnectionName = 'FQC-Authentication';

  let primaryConnection;
  for (let identity of user.identities) {
    if (identity.connection === primaryConnectionName) {
      primaryConnection = identity;
    }
  }
  if (!primaryConnection) {
    return callback(null, user, context);
  }
  const { email, email_verified } = user;
  if (!email) {
    return callback(null, user, context);
  }
  const userApiUrl = auth0.baseUrl + '/users';
  const userSearchApiUrl = auth0.baseUrl + '/users-by-email';

  request({
    url: userSearchApiUrl,
    headers: {
      Authorization: 'Bearer ' + auth0.accessToken
    },
    qs: {
      email
    }
  },
  function(err, response, body) {
    if (err) return callback(err);
    if (response.statusCode !== 200) return callback(new Error(body));

    let data = JSON.parse(body);

    let primaryUser;
    let secondaryUser;
    for (let u of data) {
      if (u.identities.length > 1) {
        console.log('[-] Skipping link rule');
        return callback(null, user, context);
      }
      for (let identity of u.identities) {
        if (identity.connection === primaryConnectionName) {
          primaryUser = u;
        } else if (identity.connection === secondaryConnectionName) {
          secondaryUser = u;
        }
      }
    }
    if (!primaryUser || !secondaryUser) {
      console.log('[-] Skipping link rule');
      return callback(null, user, context);
    }
    if (data.length === 0) {
      console.log('[-] Skipping link rule');
      return callback(null, user, context);
    }

    const provider = secondaryUser.identities[0].provider;
    const providerUserId = secondaryUser.user_id;

    request.post({
      url: userApiUrl + '/' + primaryUser.user_id + '/identities',
      headers: {
        Authorization: 'Bearer ' + auth0.accessToken
      },
      json: {
        provider: provider,
        user_id: String(providerUserId)
      }
    }, function(err, response, body) {
      if (response.statusCode >= 400) {
        return callback(new Error('Error linking account: ' + response.statusMessage));
      }
      context.primaryUser = primaryUser;
      callback(null, user, context);
    });
  });
}